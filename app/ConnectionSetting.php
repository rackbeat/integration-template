<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConnectionSetting extends Model
{
	protected $guarded = [];
}
