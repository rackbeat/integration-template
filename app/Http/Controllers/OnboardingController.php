<?php

namespace App\Http\Controllers;

use App\Connection;
use App\Rackbeat\Client;
use App\Rackbeat\Integration;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OnboardingController extends Controller
{
    public function begin( Request $request )
    {
        // 'token' is a integration request token from RB.

        if ( ! $request->has( 'token' ) ) {
            return redirect()->back();
        }

        // todo start integration onboarding. Example:
        return redirect()->away(
            'https://some-integration-onboarding-endpoint.dev?redirect_uri='
            . route( 'authorize-integration', $request->get( 'token' ) )
        );
    }

    public function authorizeIntegration( Request $request, $integrationToken )
    {
        $integrationWasSetupCorrectly = true; // todo check if the integration (oauth?) response was valid.

        if ( ! $integrationWasSetupCorrectly ) {
            // If integration was not setup or oauth denied, we will cancel the RB request
            Integration::cancel( $integrationToken );

            return response( 'Something went wrong' );
        }

        // We will now approve the integration request from RB and get our permanent API token
        try {
            $accessTokenRequest = Integration::accept( $integrationToken );
            $accessToken        = $accessTokenRequest->access_token;
            $appSlug            = $accessTokenRequest->app_slug;
            $redirectTo         = $accessTokenRequest->redirect_to ?? null;
        } catch ( Exception $exception ) {
            return response( 'Something went wrong' );
        }

        // Update an existing, or create a new connection.
        $client = Client::init( $accessToken );

        $rackbeatSelf = $client->self();

        $connection = Connection::updateOrCreate( [
            'rackbeat_user_account_id' => $rackbeatSelf->user_account->id
        ], [
            'is_active'      => 1,
            'rackbeat_token' => $accessToken,
            // todo add integration specific keys to the connection (to the connections table)
            'internal_token' => Str::random( 255 )
        ] );

        // Consider: do we need a settings page inside Rackbeat? Is an iframe. See resources/views/settings.blade.php
        // $client->setupPluginSettingsPage( $appSlug, route( 'settings-integration', [ $connection->rackbeat_user_account_id, $connection->internal_token ] ) );

        // consider: do we need webhooks?
        // $client->setupWebhook( 'model.event', route( 'webhook.model-event', [ $connection->rackbeat_user_account_id, $connection->internal_token ] ) );

        // Redirect back to Rackbeat if redirectTo is not null
        if ( $redirectTo ) {
            return redirect()->away( $redirectTo );
        }

        return response( 'ok' );
    }
}
